def func(x):
    return (3.0*x*x*x)+(2.0*x*x)+(x-15.0)

def bisection(a,b):

    if (func(a) * func(b) >= 0):
        print("You have not assumed right a and b\n")
        return

    c = a
    while ((b-a) >= 0.01):

        c = (a+b)/2


        if (func(c) == 0.0):
            break


        if (func(c)*func(a) < 0):
            b = c
        else:
            a = c

    print("The value of root is : ","%.4f"%c)

def regulaFalsi(a,b):
    i = 1
    FA = f(a)

    print("%-20s %-20s %-20s %-20s %-20s" % ("n","a_n","b_n","p_n","f(p_n)"))

    while(i <= 100):
        p = (a*f(b)-b*f(a))/(f(b) - f(a))
        FP = f(p)

        if(FP == 0 or np.abs(f(p)) < 0.01):
            break
        else:
             print("%-20.8g %-20.8g %-20.8g %-20.8g %-20.8g\n" % (i, a, b, p, f(p)))


        i = i + 1

        if(FA*FP > 0):
            a = p
        else:
            b = p

    return

a =-2
b = 8
bisection(a, b)
