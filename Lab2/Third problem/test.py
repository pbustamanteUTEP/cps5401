import math
def minussin(x, y, alpha):
    c=(x+y)/2.0
    while (y-x)/2.0 > alpha:
        if 3.0 - math.exp(c)==0:
            return c
        elif ((3.0-math.exp(x))*(3.0-math.exp(c)))<0:
            y=c
        else:
            x=c
        c=(x+y)/2.0
    return c

def trig(x, y, alpha):
    c=(x+y)/2.0
    while (y-x)/2.0 > alpha:
        if (math.sin(c)**3)-math.cos(c)==0:
            return c
        elif ((((math.sin(x)**3)-math.cos(x))*(math.sin(c)**3)-math.cos(c))) < 0:
            y=x
        else:
            x=c
        c=(x+y)/2.0
    return c

def cubic(x, y, alpha):
    c=(x+y)/2.0
    while (y-x)/2.0 > alpha:
        if (3.0*c*c*c)+(2.0*c*c)+(c-15.0)==0:
            return c
        elif (((3.0*x*x*x)+(2.0*x*x)+(x-15.0))*(3.0*c*c*c)+(2.0*c*c)+(c-15.0))<0:
            y=x
        else:
            x=c
        c=(x+y)/2.0
    return c



def gammaf(x, y, alpha):
    c=(x+y)/2.0
    while (y-x)/2.0 > alpha:
        if (math.gamma(c+0.1)-2)==0:
            return c
        elif ((math.gamma(x+0.1)-2))*(math.gamma(c+0.1)-2)<0:
            y=x
        else:
            x=c
        c=(x+y)/2.0
    return c


#def testing(x):
#	return 9.0156*x
