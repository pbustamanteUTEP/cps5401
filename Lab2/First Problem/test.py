import math
def minussin(x):
  return 3.0-math.exp(x)


def trig(x):
  return (math.sin(x)**3)-math.cos(x)


def cubic(x):
  return (3.0*x*x*x)+(2.0*x*x)+(x-15.0)


def gamma(x):
  return math.gamma(x+0.1) - 2


def testing(x):
	return 9.0156*x

