In order to execute the program I have created a makefile that you will need to execute using only by typing make and running the executable. Before opening the makefile please assure how many threads you would like to execute the problem with by establishing the number of threads using the following:
#mpirun -np [Number of ranks] ./Objective3
