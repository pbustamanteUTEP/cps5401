#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <mpi.h>

#define NB 20
extern void dgesv_(int *N, int *NRHS, double *A, int *LDA, int *IPIV, double *B, int *LDB, int *INFO);

// The basis function, j=0,...,NB
// j=0,...,NB/2,   basis is cos(jx)
// j=NB/2+1,..,NB, basis is sin((j-NB/2)x)
double ls_basis(int j, double x);

int main(int argc, char** argv) {
  MPI_Status status;
  MPI_Init( &argc, &argv );
  MPI_Comm_size( MPI_COMM_WORLD, &numprocs );
  MPI_Comm_rank( MPI_COMM_WORLD, &myid );

  int n; // Number of data pair (x_i, y_i);
  double* x;
  double* y;

  // Preparation
  assert( NB%2 == 0 );


  // Part (1) -- Read the data
  FILE* data_file = fopen("data.txt", "r");
  fscanf( data_file, "%d\n", &n );
  x = (double*) malloc( n * sizeof( double ) );
  y = (double*) malloc( n * sizeof( double ) );
  for ( int i = 0; i < n; i++ )
    fscanf( data_file, "%lf\t%lf\n", x+i, y+i );
  fclose( data_file );

  total=(NB+1)/numprocs
  total2=n/numprocs
  MPI_Bcast( &total, 1, MPI_INT, 0, MPI_COMM_WORLD );
  MPI_Bcast( &total2, 1, MPI_INT, 0, MPI_COMM_WORLD );
  // Part (2) -- Assemble the matrix
  double* A; // Size is (NB+1)*(NB+1);
  double* B; // Size is (NB+1)
  double* a; // Size is (NB+1)

  // Allocate and initialize to zero
  A = (double*) calloc( (NB+1) * (NB+1), sizeof( double ) );
  B = (double*) calloc( (NB+1), sizeof( double ) );
  a = (double*) calloc( (NB+1), sizeof( double ) );

  double* f; // Size is NB+1
  f = (double*) malloc( (NB+1) * sizeof( double ) );

  // Outer loop: iterate over each data pair
  if (myid==0){
    for ( int i = 0; i < total; i++ ) {
    // All basis functions at x_i
    for ( int j = 0; j <= total2; j++ )
      f[j] = ls_basis(j,x[i]);
    // Assemble in B
    for ( int j = 0; j <=total2; j++ )
      B[j] += y[i] * f[j];
    // Assemble in A
    for ( int j1 = 0; j1 <= total2; j1++ )
      for ( int j2 = 0; j2 <= total2; j2++ )
        A[j1*(NB+1)+j2] += f[j1] * f[j2];
  }}
  else {
    for ( int i = 0; i < total*myid; i++ ) {
    // All basis functions at x_i
    for ( int j = 0; j <= total2*myid; j++ )
      f[j] = ls_basis(j,x[i]);
    // Assemble in B
    for ( int j = 0; j <= total2*myid; j++ )
      B[j] += y[i] * f[j];
    // Assemble in A
    for ( int j1 = 0; j1 <= total2*myid; j1++ )
      for ( int j2 = 0; j2 <= total2*myid; j2++ )
        A[j1*((total2*myid)+1)+j2] += f[j1] * f[j2];
  }}
  MPI_Gather(&A, total2, MPI_DOUBLE, 1, f, numprocs, comm)
  MPI_Gather(&B, total2, MPI_DOUBLE, 1, f, numprocs, comm)
  // Part (3) -- Solve for the coefficients and report

  // Use Lapack to solve for Aa=B
  int  N    = NB+1;
  int  NRHS = 1;
  int  LDA  = NB+1;
  int* IPIV = (int*) malloc( N * sizeof( int ) );
  int  LDB  = NB+1;
  int  INFO;
  double* MAT = (double*) malloc( N * N * sizeof( double ) );
  double* RHS = (double*) malloc( N * sizeof( double ) );
  memcpy( MAT, A,  N * N * sizeof(double) );
  memcpy( RHS, B, N * sizeof(double) );
  dgbsv(&N, &NRHS, &(MAT[0]), &LDA, &(IPIV[0]), &(a[0]), &LDB, &INFO);


  // Print out coefficients in a
  printf("The fitted coefficients are:\n");
  for ( int j = 0; j <= NB; j++ )
    printf("\t%f,", a[j]);
  printf("\n");


  // Clean-up
  free( IPIV );
  free( MAT );
  free( RHS );
  free( x );
  free( y );
  free( f );

  free( A );
  free( B );
  free( a );
  MPI_Finalize();
  return 0;
}

double ls_basis(int j, double x) {
  assert( (j>=0) && (j<=NB) );
  if ( j<=NB/2 ) // cos
    return cos(j*x);
  else // sin
    return sin((j-0.5*NB)*x);
}
